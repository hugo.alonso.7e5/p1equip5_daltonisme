using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorPreguntes : MonoBehaviour
{
    public Pregunta[] Arraypreguntas;
    private string[] respuestas; //= new string[0];
    public int PreguntaActual = 0;
    private GameObject plaques;
    private GameObject opt;
    private Sprite sprite;
    private string[] opt2;
    private ControladorPreguntes controlador;
    private int numDaltonic = 0;
    public double numDaltonic2 = 0;
    public double numDaltonic3 = 0;
    private FinalResults finalResults;

    public Pregunta UpdatePregunta(int preguntaActual)
    {
        //aqui es donde tenemos que cambiar lo que lleva array preguntas 
        return Arraypreguntas[preguntaActual]; 
    }


    // Start is called before the first frame update
    void Awake()
    {
        opt = GameObject.Find("Desplega1");
        respuestas = new string[PreguntaActual];
        
    }
    public void ChangeData()
    {
        PreguntaActual++;
        opt.GetComponent<Dropdown>().options.Clear();
        List<string> m_DropOptions;
        
        Array.Resize(ref respuestas, respuestas.Length + 1);
        respuestas[respuestas.GetUpperBound(0)] = opt.GetComponent<Dropdown>().value.ToString();/*VAlor del dropdown*/
        Debug.Log(PreguntaActual);
        for (int i = 0; i < Arraypreguntas[1].posibleResp.Length; i++)
        {
            m_DropOptions = new List<string> { Arraypreguntas[i].posibleResp[PreguntaActual - 1][i] };
            opt.GetComponent<Dropdown>().AddOptions(m_DropOptions);
            opt.GetComponent<Dropdown>().options[i].text = Arraypreguntas[i].posibleResp[PreguntaActual - 1][i];

            //DropdownItemSelected(opt.GetComponent<Dropdown>()); 
            //opt.GetComponent<Dropdown>().onValueChanged.AddListener(delegate { DropdownItemSelected(opt.GetComponent<Dropdown>()); });
        }
        
    }

    public void DropdownItemSelected(Dropdown dropdown)
    {

        int index = dropdown.value;
        Arraypreguntas[2].respuestaUser = dropdown.options[index].text;

    }

    public int ComprovarRespostaUser1(string [] respuestasUser)
    {
        //Determina dalt�nic o no
        //For para las 5 primeras
        for (int i = 1; i <= 5; i++)
        {
            if (respuestasUser[i] == "0") numDaltonic += 0; 
            else if (respuestasUser[i] == "1") numDaltonic += 1;
            else numDaltonic += -1;
        }
        //For per las 5 siguientes
        for (int i = 6; i <= 5; i++)
        {
            if (respuestasUser[i] == "0") numDaltonic += 0;
            else numDaltonic += 1;
        }
        return numDaltonic;
    }
    public double ComprovarRespostaUser2(string [] respuestasUser)
    {
        //Determina tipo de daltonismo
        for (int i = 11; i <= 13; i++)
        {
            if (respuestasUser[i] == "0") numDaltonic2 += 0;
            else if (respuestasUser[i] == "1")
            {
                numDaltonic2 += 10;
                numDaltonic3 += 10;
            }
            else if (respuestasUser[i] == "4")
            {
                numDaltonic2 += 10;
                numDaltonic3 += 0;
            }
            else if (respuestasUser[i] == "2")
            {
                numDaltonic2 += -10;
                numDaltonic3 += 10;
            }
            else if (respuestasUser[i] == "3")
            {
                numDaltonic2 += -10;
                numDaltonic3 += 0;
            }
            
        }

        if (respuestasUser[14] == "0") numDaltonic2 += 0;
        if (respuestasUser[14] == "1") numDaltonic2 += 10;
        if (respuestasUser[14] == "2") numDaltonic2 += -10;

        return numDaltonic2;
    }
    public double ComprovarRespostaUser3(string[] respuestasUser)
    {
        //Determina grau de daltonismo
        for (int i = 15; i <= 17; i++)
        {
            if (respuestasUser[i] == "0") numDaltonic3 += 0; 
            if (respuestasUser[i] == "1" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
            if (respuestasUser[i] == "2" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
        }

        for (int i = 18; i <= 19; i++)
        {
            if (respuestasUser[i] == "0") numDaltonic3 += 0;
            if (respuestasUser[i] == "1" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
            if (respuestasUser[i] == "2" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
            if (respuestasUser[i] == "3" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
        }

        for (int i = 20; i <= 21; i++)
        {
            if (respuestasUser[i] == "0") numDaltonic3 += 0;
            if (respuestasUser[i] == "1" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
            if (respuestasUser[i] == "2" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
        }

        if (respuestasUser[22] == "0") numDaltonic3 += 0;
        if (respuestasUser[22] == "1" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
        if (respuestasUser[22] == "2" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
        if (respuestasUser[22] == "3" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;

        for (int i = 23; i <= 24; i++)
        {
            if (respuestasUser[i] == "0") numDaltonic3 += 0;
            if (respuestasUser[i] == "1" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
            if (respuestasUser[i] == "2" && numDaltonic3 <= 28.5) numDaltonic3 += 1.5;
        }

        if (respuestasUser[25] == "0" || respuestasUser[25] == "1") numDaltonic3 += 0;
        return numDaltonic3;
    }
    public void ControlarSeguimentTest()
    {
        //Llamar a funcion que comprueba la respuestaUser (sumamos 0 respuesta OK, +1 respuesta Daltonica i -1 No veig res)
        numDaltonic = controlador.ComprovarRespostaUser1(respuestas);

        //Devolver el int que usaremos para filtrar el OK o el NOT OK
        if (numDaltonic == 0) finalResults.DeterminarResultat(0);
        else if (numDaltonic <= -4) finalResults.DeterminarResultat(-1);
    }

    public void SeleccionarTipoIGrau(double numDaltonic2, double numDaltonic3)
    {
        //Llamar a funcion que comprueba la respuestaUser2 (porque tenemos que flitrar por el tipo de daltonismo preguntas 11 a la 14 )
        numDaltonic2 = controlador.ComprovarRespostaUser2(respuestas);
        numDaltonic3 = controlador.ComprovarRespostaUser3(respuestas);
        //Tenemos que hacer un filtro para determinar tipo segun la respuesta
        if (numDaltonic2 == 0) finalResults.DeterminarResultat(0);
        if (numDaltonic2 > 0) finalResults.DeterminarResultat(2);
        if (numDaltonic2 < 0) finalResults.DeterminarResultat(1);

        //Por cada respuesta de tipo greu (mirar cuadro del documento), sumaremos 7.5 a un int que utilizaremos luego para el slider
    }

    public int SeleccionarGrau()
    {
        //A partir de la pregunta 15 hasta la 25, sumaremos 1.5 por cada respuesta Daltonica, partiendo del numero que recibamos del tipo
        //El slider de grau de los resultados ire concorde el numero recibido al acabar la pregunta 25, que sera del 0 al 30 (siendo 0 daltonismo muy leve i siendo 30 daltonismo muy grave)
        return 0;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
