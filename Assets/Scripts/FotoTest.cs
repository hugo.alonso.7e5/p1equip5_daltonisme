using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FotoTest : MonoBehaviour
{

    public Sprite [] Discos;
    public int QuestionNum = 1;
    private ControladorPreguntes controladorPreguntes;
    private NumeroPregunta numeroPregunta; 
    private int NumTipo;
    private int NumGrau;
    private FinalResults finalResults;
    //public Text NumPregunta;

    public void SeguentClickButton()
    {
        this.gameObject.GetComponent<Image>().sprite = Discos[QuestionNum];

        if (numeroPregunta.QuestionNum == 10) controladorPreguntes.ControlarSeguimentTest();
        
        if (numeroPregunta.QuestionNum > 25)
        {
            controladorPreguntes.SeleccionarTipoIGrau(controladorPreguntes.numDaltonic2, controladorPreguntes.numDaltonic3);
        }
        QuestionNum++;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
