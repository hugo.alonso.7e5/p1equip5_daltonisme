using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumeroPregunta : MonoBehaviour
{

    public Text NumPregunta;
    public int QuestionNum = 1;
    
    public void ClickSeguent()
    {
        NumPregunta.text = (QuestionNum).ToString();
        QuestionNum++;
    }

}
