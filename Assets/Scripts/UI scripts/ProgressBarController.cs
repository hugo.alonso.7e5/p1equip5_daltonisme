using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarController : MonoBehaviour
{
    public ControladorPreguntes Controlador;
    public Slider slider;
    public NumeroPregunta numeroPregunta;
    public int progress;
    
    public void UpdateProgress()
    {
        progress = numeroPregunta.QuestionNum;
        slider.value = progress;
    }

}
