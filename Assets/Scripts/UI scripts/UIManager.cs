using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject Dropdown;
    public GameObject Results;
    public GameObject HelpScreen;
    public ControladorPreguntes Controlador;
    public GameObject Menu; // arrastrar la imagen del menu
    //public Dropdown dropdown1;


    private GameObject _Foto;

    public void SeguentImatge()
    {
        Controlador.PreguntaActual++;



    }

    public void BackToMenu()
    {

        Dropdown.SetActive(false);
        Menu.SetActive(true);
        //resetear progreso
    }

    public void PulsaStart()
    {
        Dropdown.SetActive(true);

    }

    //S'OBRE LA FINESTRA HELP
    public void PulsaHelp()
    {
       HelpScreen.SetActive(true);
    }

    //Pulsa la Creu a la finestra HELP
    public void SurtHelp()
    {
        HelpScreen.SetActive(false);
    }

    //Surt de la APP
    public void PulsaExit()
    {
        Application.Quit();
    }

    public void MostrarPantallaResultat()
    {
        Dropdown.SetActive(false);
        Results.SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        Dropdown.SetActive(false);
        Results.SetActive(false);
        HelpScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
