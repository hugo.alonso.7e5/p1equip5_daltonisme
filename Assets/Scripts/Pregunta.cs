using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pregunta
{
    public Sprite Discos;
    public string[][] posibleResp = new string[][] {
        new string [] {"12", "No veig res"},
        new string [] {"8" , "3" , "No veig res"},
        new string [] {"6", "5" , "No veig res"},
        new string [] {"5", "2" , "No veig res"},
        new string [] {"74" , "21" , "No veig res"},

        new string [] {"2" , "Veig un altre numero" , "No veig res"},
        new string [] {"97" , "Veig un altre numero" , "No veig res"},
        new string [] {"7" , "Veig un altre numero" , "No veig res"},
        new string [] {"73" , "Veig un altre numero" , "No veig res"},
        new string [] {"26" , "6" , "2", "No veig res"},

        new string [] {"42" , "2" , "4", "4 i ligerament un 2", "2 i ligerament un 4"},
        new string [] {"35" , "5" , "3", "3 i ligerament un 5", "5 i ligerament un 3"},
        new string [] {"96" , "6" , "9", "9 i ligerament un 6", "6 i ligerament un 9"},
        new string [] {"Dues linies" , "Una linia lila" , "Una linia vermella", "Altres"},
        
        new string [] {"57" , "35", "No veig res"},
        new string [] {"45" , "Veig un altre numero", "No veig res"},
        new string [] {"16" , "Veig un altre numero", "No veig res"},
        
        new string [] {"Res coherent" , "5",  "Una linia marcada", "No veig res"},
        new string [] {"Res coherent" , "2", "Una linia marcada", "No veig res"},

        new string [] {"Res coherent" , "Una linia marcada", "No veig res"},
        new string [] {"Un cami verd/blau" , "Res coherent", "Altres"},
        
        new string [] {"Un cami taronja" , "Una linia marcada", "Res coherent", "Altres"},

        new string [] {"Un cami verd" , "Una cami lila", "Altres"},
        new string [] {"Un cami taronja" , "Una cami blau/verd", "Altres"},

        new string [] {"Un cami taronja" , "Altres"} };
    
    public string respuestaUser; 

}
