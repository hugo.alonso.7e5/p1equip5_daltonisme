using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalResults : MonoBehaviour
{
    private UIManager Manager;
    public Sprite[] Tipo;
    public Slider GradeSlider;
    public ControladorPreguntes controlador;
    private string TipoText; //(Li asignarem el nom del sprite amb la foto del tipus de daltonisme)

    public void DeterminarResultat(int NumTipo)
    {
        Manager.MostrarPantallaResultat();
        DeterminarFotoITextTipo(NumTipo);
      
        GradeSlider.value = (float)controlador.numDaltonic3;
    }

    public void DeterminarFotoITextTipo(int NumTipo)
    {
        if (NumTipo == 0)
        {
            this.gameObject.GetComponent<Image>().sprite = Tipo[0];
            Tipo[0].name = TipoText;
        }
        else if (NumTipo == -1)
        {
            this.gameObject.GetComponent<Image>().sprite = Tipo[1];
            Tipo[1].name = TipoText;
        }
        else if (NumTipo == 2)
        {
            this.gameObject.GetComponent<Image>().sprite = Tipo[2];
            Tipo[2].name = TipoText;
        }
        else if (NumTipo == 1)
        {
            this.gameObject.GetComponent<Image>().sprite = Tipo[3];
            Tipo[3].name = TipoText;
        }
    }
}
